package com.eagb.androidnativeexercise.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.eagb.androidnativeexercise.R;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class AboutDialog extends DialogFragment implements View.OnClickListener {

	private Button btnClose;

	public AboutDialog() {
		// Required empty public constructor
	}

	public static AboutDialog newInstance() {
		return new AboutDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.dialog_about, container, false);

		btnClose = rootView.findViewById(R.id.btn_close);

		return rootView;
	}

	@Override
	public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		btnClose.setOnClickListener(this);
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		return dialog;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (getDialog() == null) {
			return;
		}
		getDialog().getWindow().setWindowAnimations(
				R.style.dialogAnimationEnterUpExitBotton);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.btn_close: // Closing the dialog
				dismiss();
				break;
		}
	}

}
