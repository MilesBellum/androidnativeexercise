package com.eagb.androidnativeexercise.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.eagb.androidnativeexercise.MainActivity;
import com.eagb.androidnativeexercise.R;
import com.eagb.androidnativeexercise.adapters.SpinnerNoHintAdapter;
import com.eagb.androidnativeexercise.managers.SharedPreferencesManager;
import com.eagb.androidnativeexercise.utils.LocaleUtils;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class LanguageDialog extends DialogFragment implements View.OnClickListener {

	private Spinner spinnerLanguage;
	private Button btnAccept, btnCancel;

	public LanguageDialog() {
		// Required empty public constructor
	}

	public static LanguageDialog newInstance() {
		return new LanguageDialog();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.dialog_language, container, false);

		SharedPreferencesManager prefs = new SharedPreferencesManager(getContext());

		btnAccept = rootView.findViewById(R.id.btn_accept);
		btnCancel = rootView.findViewById(R.id.btn_cancel);
		spinnerLanguage = rootView.findViewById(R.id.spinner_language);

		// Setting the languages data into Spinner
		SpinnerNoHintAdapter spinnerLanguageAdapter = new SpinnerNoHintAdapter(getActivity(), R.layout.spinner_item);
		spinnerLanguageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerLanguageAdapter.addAll(getResources().getStringArray(R.array.languages));
		spinnerLanguage.setAdapter(spinnerLanguageAdapter);
		spinnerLanguage.setSelection(spinnerLanguageAdapter.getCount());

		// Selecting the right item in the language spinner
		switch(prefs.getLanguage()){
			case "system":
				spinnerLanguage.setSelection(0);
				break;
			case "en":
				spinnerLanguage.setSelection(1);
				break;
			case "es":
				spinnerLanguage.setSelection(2);
				break;
		}

		return rootView;
	}

	@Override
	public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		getSelectedLanguageItem(); // Getting the selected language item

		btnAccept.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	private void getSelectedLanguageItem() {
		spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
			                           int position, long id) {
				switch(position){
					case 0:
						LocaleUtils.setLocale(getContext(), "system"); // Default (System setting)
						break;
					case 1:
						LocaleUtils.setLocale(getContext(), "en"); // en-US English, United States
						break;
					case 2:
						LocaleUtils.setLocale(getContext(), "es"); // es-MX Spanish, Mexico
						break;
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing
			}
		});
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		return dialog;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (getDialog() == null) {
			return;
		}
		getDialog().getWindow().setWindowAnimations(
				R.style.dialogAnimationEnterUpExitBotton);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.btn_accept: // Accepting the changes
				settingLanguage();
				break;
			case R.id.btn_cancel: // Canceling the dialog
				dismiss();
				break;
		}
	}

	// Setting the language selected
	private void settingLanguage() {
		Intent refresh = new Intent(getActivity(), MainActivity.class);
		startActivity(refresh);
		dismiss();
	}
}
