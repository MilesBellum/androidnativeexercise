package com.eagb.androidnativeexercise.managers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.eagb.androidnativeexercise.utils.ConstantsUtils;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class SharedPreferencesManager {

	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;

	@SuppressLint("CommitPrefEdits")
	public SharedPreferencesManager(Context context){
		sharedPreferences = context.getSharedPreferences(ConstantsUtils.PREFERENCES_DATA, Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();
	}

	public void setLanguage(String currentLanguage) {
		editor.putString(ConstantsUtils.LANGUAGE_SELECTED, currentLanguage);
		editor.commit();
	}

	public String getLanguage() {
		return sharedPreferences.getString(ConstantsUtils.LANGUAGE_SELECTED, "system");
	}
}