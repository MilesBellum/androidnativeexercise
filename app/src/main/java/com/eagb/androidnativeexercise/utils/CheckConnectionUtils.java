package com.eagb.androidnativeexercise.utils;

import android.content.Context;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.eagb.androidnativeexercise.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Edgar Garcia on 26/04/2019.
 */

public class CheckConnectionUtils {

	private Context mContext;
	private View view;
	private Timer timer;
	private TimerTask timerTask;

	public CheckConnectionUtils(Context context, View view){
		this.mContext = context;
		this.view = view;
	}

	public void startTimer() {
		timer = new Timer();
		initializeTimerTask();
		int milliseconds = 2000; // 2 seconds
		timer.schedule(timerTask, 0, milliseconds);
	}

	private void stopTimerTask() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	private void initializeTimerTask() {
		timerTask = new TimerTask() {
			final Handler handler = new Handler();
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						if (NetworkUtils.isNetworkAvailable(mContext)){
							// Connected to Internet
							Snackbar.make(view, R.string.text_connected_to_internet, Snackbar.LENGTH_SHORT)
									.setAction("Action", null).show();

							stopTimerTask(); // Stops the timer
						}
					}
				});
			}
		};
	}
}
