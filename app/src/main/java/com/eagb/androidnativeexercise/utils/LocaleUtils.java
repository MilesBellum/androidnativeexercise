package com.eagb.androidnativeexercise.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.util.Locale;

import com.eagb.androidnativeexercise.managers.SharedPreferencesManager;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class LocaleUtils {

	public static Context onAttach(Context context) {
		String locale = getPersistedLocale(context);
		return setLocale(context, locale);
	}

	public static String getPersistedLocale(Context context) {
		SharedPreferencesManager prefs = new SharedPreferencesManager(context);
		return prefs.getLanguage();
	}

	/**
	 * Set the app's locale to the one specified by the given String.
	 *
	 * @param context from is called
	 * @param localeSelected a locale specification as used for Android resources (NOTE: does not
	 *                   support country and variant codes so far); the special string "system" sets
	 *                   the locale to the locale specified in system settings
	 * @return the context and locale
	 */
	public static Context setLocale(Context context, String localeSelected) {
		SharedPreferencesManager prefs = new SharedPreferencesManager(context);
		prefs.setLanguage(localeSelected); // Saving the set language in Shared Preferences

		Locale locale;
		if (localeSelected.equals("system")) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
				locale = Resources.getSystem().getConfiguration().getLocales().get(0);
			} else {
				// No inspection deprecation
				locale = Resources.getSystem().getConfiguration().locale;
			}
		} else {
			locale = new Locale(localeSelected);
		}
		Locale.setDefault(locale);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			return updateResources(context, locale);
		} else {
			return updateResourcesLegacy(context, locale);
		}
	}

	@TargetApi(Build.VERSION_CODES.N)
	private static Context updateResources(Context context, Locale locale) {
		Configuration configuration = context.getResources().getConfiguration();
		configuration.setLocale(locale);
		configuration.setLayoutDirection(locale);

		return context.createConfigurationContext(configuration);
	}

	@SuppressWarnings("deprecation")
	private static Context updateResourcesLegacy(Context context, Locale locale) {
		Resources resources = context.getResources();
		Configuration configuration = resources.getConfiguration();
		configuration.locale = locale;
		configuration.setLayoutDirection(locale);
		resources.updateConfiguration(configuration, resources.getDisplayMetrics());

		return context;
	}
}