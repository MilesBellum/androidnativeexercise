package com.eagb.androidnativeexercise.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public abstract class BaseActivity extends AppCompatActivity {
	private String initialLocale;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialLocale = LocaleUtils.getPersistedLocale(this);
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleUtils.onAttach(base));
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (initialLocale != null && !initialLocale.equals(LocaleUtils.getPersistedLocale(this))) {
			recreate();
		}
	}
}