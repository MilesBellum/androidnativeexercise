package com.eagb.androidnativeexercise.utils;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class ConstantsUtils {

	public ConstantsUtils() { }

	// URLs
	public static final String URL_BASE_FOR_POPULAR = "https://api.themoviedb.org/3/movie/popular?api_key=6e27cce4970dc858b2339afc24464fd6&page=1";
	public static final String URL_BASE_FOR_RATED = "https://api.themoviedb.org/3/movie/top_rated?api_key=6e27cce4970dc858b2339afc24464fd6&page=1";
	public static final String URL_BASE_FOR_IMAGE = "https://image.tmdb.org/t/p/w300";
	public static final String URL_BASE_FOR_LARGE_IMAGE = "https://image.tmdb.org/t/p/w500";
	public static final String A_BEAUTIFUL_SURPRISE = "market://details?id=eagb.projectpizza.pro";

	// API Keys
	public static final String MOVIE_ID = "id";
	public static final String MOVIE_COVER = "poster_path";
	public static final String MOVIE_TITLE = "title";
	public static final String MOVIE_SYNOPSIS = "overview";
	public static final String USER_RATING = "vote_average";
	public static final String MOVIE_RELEASE_DATE = "release_date";

	// Integers
	public static final int GRID_COLUMNS = 3;

	// String
	public static final String PREFERENCES_DATA = "eagb.androidnativeexercise"; // Package data
	public static final String LANGUAGE_SELECTED = "language_selected"; // App language
	public static final String LANGUAGE = "language"; // App language
	public static final String ABOUT = "about"; // About
}
