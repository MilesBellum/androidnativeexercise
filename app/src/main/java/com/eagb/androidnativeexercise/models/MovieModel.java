package com.eagb.androidnativeexercise.models;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class MovieModel {

	private String movieId, movieCover, movieTitle, movieSynopsis, userRating, movieReleaseDate;

	public MovieModel() {}

	public MovieModel(String movieId, String movieCover, String movieTitle, String movieSynopsis, String userRating, String movieReleaseDate) {
		this.movieId = movieId;
		this.movieCover = movieCover;
		this.movieTitle = movieTitle;
		this.movieSynopsis = movieSynopsis;
		this.userRating = userRating;
		this.movieReleaseDate = movieReleaseDate;
	}

	// Getters methods
	public String getMovieId() {
		return movieId;
	}

	public String getMovieCover() {
		return movieCover;
	}

	public String getMovieTitle() {
		return movieTitle;
	}

	public String getMovieSynopsis() {
		return movieSynopsis;
	}

	public String getUserRating() {
		return userRating;
	}

	public String getMovieReleaseDate() {
		return movieReleaseDate;
	}

	// Setters methods
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}

	public void setMovieCover(String movieCover) {
		this.movieCover = movieCover;
	}

	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}

	public void setMovieSynopsis(String movieSynopsis) {
		this.movieSynopsis = movieSynopsis;
	}

	public void setUserRating(String userRating) {
		this.userRating = userRating;
	}

	public void setMovieReleaseDate(String movieReleaseDate) { this.movieReleaseDate = movieReleaseDate; }
}
