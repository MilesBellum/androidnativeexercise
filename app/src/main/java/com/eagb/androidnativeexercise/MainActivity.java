package com.eagb.androidnativeexercise;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.eagb.androidnativeexercise.adapters.RecyclerViewAdapter;
import com.eagb.androidnativeexercise.dialogs.AboutDialog;
import com.eagb.androidnativeexercise.dialogs.LanguageDialog;
import com.eagb.androidnativeexercise.models.MovieModel;
import com.eagb.androidnativeexercise.utils.BaseActivity;
import com.eagb.androidnativeexercise.utils.CheckConnectionUtils;
import com.eagb.androidnativeexercise.utils.ConstantsUtils;
import com.eagb.androidnativeexercise.utils.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Edgar Garcia on 26/04/2019.
 */

public class MainActivity extends BaseActivity {

	private static final String TAG = "MainActivity";
	private CheckConnectionUtils mCheckConnectionUtils;
	private RequestQueue mRequestQueue;
	private ProgressDialog mProgressDialog;
	private TextView txtListEmpty;
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private RecyclerView mRecyclerView;
	private boolean isChecked = true;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.title_activity_movies); // Activity title in toolbar
		mCheckConnectionUtils = new CheckConnectionUtils(this, findViewById(android.R.id.content));
		txtListEmpty = findViewById(R.id.text_list_empty);
		mSwipeRefreshLayout = findViewById(R.id.swipe_container);
		mRecyclerView = findViewById(R.id.recycler_movies);

		startRequestQueue(true);

		mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
				android.R.color.holo_green_dark,
				android.R.color.holo_orange_dark,
				android.R.color.holo_blue_dark);
		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				startRequestQueue(isChecked);
			}
		});

		// A beautiful surprise!
		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantsUtils.A_BEAUTIFUL_SURPRISE));
					startActivity(intent);
				}catch (ActivityNotFoundException e){
					e.getStackTrace();
					Toast.makeText(MainActivity.this, R.string.error_activity_get_pro_not_found, Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void internetConnectionLost() {
		Snackbar.make(findViewById(android.R.id.content), R.string.error_internet_connection_lost, Snackbar.LENGTH_INDEFINITE)
				.setAction(R.string.text_check_out, new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
					}
				}).show();

		mCheckConnectionUtils.startTimer(); // Starting a timer to check Internet
	}

	private void startRequestQueue(boolean isPopularList) {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage(getApplicationContext().getResources().getString(R.string.text_loading));
		mProgressDialog.show();
		mSwipeRefreshLayout.setRefreshing(true);

		if (NetworkUtils.isNetworkAvailable(this)) {
			mRequestQueue = Volley.newRequestQueue(this); // Instantiate the RequestQueue

			// Request a string response from the provided URL
			StringRequest stringRequest;
			if (isPopularList) {
				stringRequest = new StringRequest(Request.Method.GET, ConstantsUtils.URL_BASE_FOR_POPULAR,
						new Response.Listener<String>() {
							@Override
							public void onResponse(String response) {
								Log.d(TAG, "Response success");
								getMoviesData(response); // Get movies from server and setting in RecyclerView
								txtListEmpty.setVisibility(View.GONE);
							}
						}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(TAG, "Response failure");
						txtListEmpty.setVisibility(View.VISIBLE);
						mProgressDialog.dismiss();
						mSwipeRefreshLayout.setRefreshing(false);
					}
				});
			} else {
				stringRequest = new StringRequest(Request.Method.GET, ConstantsUtils.URL_BASE_FOR_RATED,
						new Response.Listener<String>() {
							@Override
							public void onResponse(String response) {
								Log.d(TAG, "Response success");
								getMoviesData(response); // Get movies from server and setting in RecyclerView
								txtListEmpty.setVisibility(View.GONE);
							}
						}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(TAG, "Response failure");
						txtListEmpty.setVisibility(View.VISIBLE);
						mProgressDialog.dismiss();
						mSwipeRefreshLayout.setRefreshing(false);
					}
				});
			}

			stringRequest.setTag(TAG); // Set the tag on the request

			mRequestQueue.add(stringRequest); // Add the request to the RequestQueue
		} else {
			internetConnectionLost();
		}
	}

	@Override
	protected void onStop () {
		super.onStop();
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(TAG);
		}
	}

	// Getting movies from server and setting in RecyclerView
	private void getMoviesData(String response) {
		if (response != null) {
			mSwipeRefreshLayout.setRefreshing(false);
			List<MovieModel> listMovie = new ArrayList<>();

			try {
				JSONObject jsonObject = new JSONObject(response);
				JSONArray jsonArray = jsonObject.getJSONArray("results");
				Log.d(TAG, "Response: " + jsonArray);
				for (int i = 0; i < jsonArray.length(); i++){
					listMovie.add(new MovieModel(
							jsonArray.getJSONObject(i).getString(ConstantsUtils.MOVIE_ID),
							jsonArray.getJSONObject(i).getString(ConstantsUtils.MOVIE_COVER),
							jsonArray.getJSONObject(i).getString(ConstantsUtils.MOVIE_TITLE),
							jsonArray.getJSONObject(i).getString(ConstantsUtils.MOVIE_SYNOPSIS),
							jsonArray.getJSONObject(i).getString(ConstantsUtils.USER_RATING),
							jsonArray.getJSONObject(i).getString(ConstantsUtils.MOVIE_RELEASE_DATE))
					);

					RecyclerViewAdapter adapter = new RecyclerViewAdapter(MainActivity.this, listMovie);
					mRecyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, ConstantsUtils.GRID_COLUMNS));
					mRecyclerView.setAdapter(adapter);
				}

				mProgressDialog.dismiss();
			}catch (JSONException e){
				e.printStackTrace();
				Log.e(TAG, "Server is down");
				txtListEmpty.setVisibility(View.VISIBLE);
				Snackbar.make(findViewById(android.R.id.content), R.string.error_server_connection_lost, Snackbar.LENGTH_SHORT)
						.setAction("Action", null).show();
				mProgressDialog.dismiss();
			}
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem checkable = menu.findItem(R.id.action_order_filter);
		checkable.setChecked(isChecked);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_order_filter:
				if (NetworkUtils.isNetworkAvailable(this)) {
					isChecked = !item.isChecked();
					item.setChecked(isChecked);
					if (item.isChecked()) {
						Toast.makeText(this, R.string.text_most_popular, Toast.LENGTH_LONG).show();
						startRequestQueue(true); // Filtered by most popular
					} else {
						Toast.makeText(this, R.string.text_higher_audience, Toast.LENGTH_LONG).show();
						startRequestQueue(false); // Filtered by higher audience
					}
					return true;
				} else {
					internetConnectionLost();
					return false;
				}

			case R.id.action_language:
				LanguageDialog languageDialog = LanguageDialog.newInstance();
				languageDialog.show(this.getSupportFragmentManager(), ConstantsUtils.LANGUAGE);
				return true;

			case R.id.action_about:
				AboutDialog aboutDialog = AboutDialog.newInstance();
				aboutDialog.show(this.getSupportFragmentManager(), ConstantsUtils.ABOUT);
				return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
