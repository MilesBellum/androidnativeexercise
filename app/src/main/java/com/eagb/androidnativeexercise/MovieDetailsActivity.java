package com.eagb.androidnativeexercise;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eagb.androidnativeexercise.utils.BaseActivity;
import com.eagb.androidnativeexercise.utils.CheckConnectionUtils;
import com.eagb.androidnativeexercise.utils.ConstantsUtils;
import com.eagb.androidnativeexercise.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.Objects;

/**
 * Created by Edgar Garcia on 26/04/2019.
 */

public class MovieDetailsActivity extends BaseActivity {

	private static final String TAG = "MovieDetailsActivity";
	private CheckConnectionUtils mCheckConnectionUtils;

	@SuppressLint("SetTextI18n")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movie_details);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// Setting Back button and activity title in toolbar
		assert getSupportActionBar() != null;
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.title_activity_movie_details);
		mCheckConnectionUtils = new CheckConnectionUtils(this, findViewById(android.R.id.content));

		ImageView imgMovieCoverHeader = findViewById(R.id.img_movie_cover_header);
		TextView txtMovieTitle = findViewById(R.id.txt_movie_title);
		TextView txtMovieReleaseDate = findViewById(R.id.txt_movie_release_date);
		TextView txtUserRating = findViewById(R.id.txt_user_rating);
		TextView txtMovieSynopsis = findViewById(R.id.txt_movie_synopsis);

		Intent intent = getIntent();
		String movieCover = intent.getExtras().getString(ConstantsUtils.MOVIE_COVER);
		String movieTitle = intent.getExtras().getString(ConstantsUtils.MOVIE_TITLE);
		String movieReleaseDate = intent.getExtras().getString(ConstantsUtils.MOVIE_RELEASE_DATE);
		String userRating = intent.getExtras().getString(ConstantsUtils.USER_RATING);
		String movieSynopsis = intent.getExtras().getString(ConstantsUtils.MOVIE_SYNOPSIS);

		if (NetworkUtils.isNetworkAvailable(this)) {
			Picasso.get().load(ConstantsUtils.URL_BASE_FOR_LARGE_IMAGE + movieCover).into(imgMovieCoverHeader);
		} else {
			internetConnectionLost();
		}
		txtMovieTitle.setText(movieTitle);
		txtMovieReleaseDate.setText(movieReleaseDate);
		txtUserRating.setText(userRating + getResources().getString(R.string.text_ten_score));
		txtMovieSynopsis.setText(movieSynopsis);
	}

	private void internetConnectionLost() {
		Snackbar.make(findViewById(android.R.id.content), R.string.error_internet_connection_lost, Snackbar.LENGTH_INDEFINITE)
				.setAction(R.string.text_check_out, new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
					}
				}).show();

		mCheckConnectionUtils.startTimer(); // Starting a timer to check Internet
	}

	@Override
	public boolean onSupportNavigateUp(){
		finish();
		return true;
	}
}
