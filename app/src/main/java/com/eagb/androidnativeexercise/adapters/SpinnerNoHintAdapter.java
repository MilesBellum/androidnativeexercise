package com.eagb.androidnativeexercise.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class SpinnerNoHintAdapter extends ArrayAdapter<String> {

	public SpinnerNoHintAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	@Override
	public int getCount() {
		int count = super.getCount();
		return count > 0 ? count : count ;
	}
}