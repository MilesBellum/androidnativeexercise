package com.eagb.androidnativeexercise.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.eagb.androidnativeexercise.MovieDetailsActivity;
import com.eagb.androidnativeexercise.R;
import com.eagb.androidnativeexercise.models.MovieModel;
import com.eagb.androidnativeexercise.utils.ConstantsUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Edgar Garcia on 28/04/2019.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

	private Context mContext;
	private List<MovieModel> mMovieData;

	public RecyclerViewAdapter(Context mContext, List<MovieModel> mMovieData) {
		this.mContext = mContext;
		this.mMovieData = mMovieData;
	}

	@NonNull
	@Override
	public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

		View view;
		LayoutInflater mInflater = LayoutInflater.from(mContext);

		view = mInflater.inflate(R.layout.item_movie_cover, viewGroup, false);

		return new MyViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
		Picasso.get().load(ConstantsUtils.URL_BASE_FOR_IMAGE + mMovieData.get(position).getMovieCover()).into(holder.imgMovieCover);
		holder.cardMovieCover.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, MovieDetailsActivity.class);
				intent.putExtra(ConstantsUtils.MOVIE_ID, mMovieData.get(position).getMovieId());
				intent.putExtra(ConstantsUtils.MOVIE_COVER, mMovieData.get(position).getMovieCover());
				intent.putExtra(ConstantsUtils.MOVIE_TITLE, mMovieData.get(position).getMovieTitle());
				intent.putExtra(ConstantsUtils.MOVIE_SYNOPSIS, mMovieData.get(position).getMovieSynopsis());
				intent.putExtra(ConstantsUtils.USER_RATING, mMovieData.get(position).getUserRating());
				intent.putExtra(ConstantsUtils.MOVIE_RELEASE_DATE, mMovieData.get(position).getMovieReleaseDate());
				mContext.startActivity(intent);
			}
		});
	}

	@Override
	public int getItemCount() {
		return mMovieData.size();
	}

	static class MyViewHolder extends RecyclerView.ViewHolder {

		ImageView imgMovieCover;
		CardView cardMovieCover;

		MyViewHolder(@NonNull View itemView) {
			super(itemView);

			imgMovieCover = itemView.findViewById(R.id.img_image_cover);
			cardMovieCover = itemView.findViewById(R.id.card_image_cover);
		}
	}
}
